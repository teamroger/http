<?php declare(strict_types=1);

namespace Rmb32\Http\Tests\Unit\Request;

use Rmb32\Http\Header;
use PHPUnit\Framework\TestCase;

class HeaderTest extends TestCase
{
    /**
    * @test
    */
    public function constructs_successfully() : void
    {
        $header = new Header('Content-type', 'text/html');
        
        $this->assertInstanceOf(Header::class, $header);
    }
}
