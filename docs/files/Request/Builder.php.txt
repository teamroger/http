<?php
/**
  * Class: Builder | src/Request/Builder.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Request;

use Rmb32\Http\Request;
use Rmb32\Http\Contracts\RequestSource;

/**
 * Class for building HTTP requests based on a source of data for the request.
 *
 * @package     Rmb32\Http
 * @subpackage  Request
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class Builder
{
    /**
     * Creates a request object.
     *
     * @param \Rmb32\Http\Contracts\RequestSource $source The request source.
     * @return Request A new request object.
     */
    public function build(RequestSource $source) : Request
    {
        return Request::create(
            $source->getMethod(),
            $source->getUri(),
            $source->getQueryString(),
            $source->getHttpVersion(),
            $source->getHeaders(),
            $source->getBody()
        );
    }
}

