<?php
/**
  * Class: Method | src/Request/Method.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Request;

use Rmb32\Http\Exceptions\HttpException;

/**
 * Class to represent an HTTP request method e.g.- "GET" o r "POST".
 *
 * @package     Rmb32\Http
 * @subpackage  Request
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class Method
{
    /**
     * Constructs a new HTTP method.
     *
     * @param string $method The method.
     * @throw \Rmb32\Http\Exceptions\HttpException If the method is invalid.
     */
    public function __construct(string $method)
    {
        $upperMethod = strtoupper($method);
        
        if (!in_array(
            $upperMethod,
            [
                'GET', 'POST', 'PUT', 'DELETE',
                'HEAD', 'TRACE', 'OPTIONS', 'CONNECT'
            ]
        )) {
            throw new HttpException(
                'Invalid HTTP request method: "' . $upperMethod . '"'
            );
        }
        
        $this->method = $upperMethod;
    }
    
    /**
     * Gets a string representation of this HTTP method object.
     *
     * @return string A string representation of this object.
     */
    public function __toString() : string
    {
        return $this->method;
    }
}

