<?php
/**
  * Interface RequestSource | src/Contracts/RequestSource.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Contracts;

use Rmb32\Http\Request\Method;
use Rmb32\Http\Header\HeaderCollection;

/**
 * Interface to represent the source of an HTTP request.
 * You may not always want to deal with requests produced from
 * the PHP superglobals. For example, you may want to read a request
 * from a text file or from a command terminal.
 *
 * @package     Rmb32\Http
 * @subpackage  Contracts
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
interface RequestSource
{
    /**
     * Gets the HTTP request method.
     *
     * @return string The request method.
     */
    public function getMethod() : string;
    
    /**
     * Gets the requested URI, not including the query string.
     *
     * @return string The request URI, not including the query string.
     */
    public function getUri() : string;
    
    /**
     * Gets the query string.
     *
     * @return string The query string.
     */
    public function getQueryString() : string;
    
    /**
     * Gets the HTTP version.
     *
     * @return string The HTTP version.
     */
    public function getHttpVersion() : string;
    
    /**
     * Gets the HTTP headers.
     *
     * @return \Rmb32\Http\Header\HeaderCollection The HTTP headers.
     */
    public function getHeaders() : HeaderCollection;
    
    /**
     * Gets the request body (posted data).
     *
     * @return string The request body.
     */
    public function getBody() : string;
}
