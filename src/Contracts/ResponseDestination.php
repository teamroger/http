<?php
/**
  * Interface ResponseDestination | src/Contracts/ResponseDestination.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Contracts;

use Rmb32\Http\Response;

/**
 * An interface to represent an output destination for an HTTP response.
 * You might not always want to send a response directly to an HTTP client.
 * For example, you may wish to send it to a text file, a command terminal or
 * a database.
 *
 * @package     Rmb32\Http
 * @subpackage  Contracts
 * @author      Rogerr Barnfather <roger@rogerbarnfather.com>
 */
interface ResponseDestination
{
    /**
     * Sends the HTTP response to the desination.
     *
     * @param \Rmb32\Http\Response $response The HTTP response.
     * @return void
     */
    public function accept(Response $response) : void;
}
