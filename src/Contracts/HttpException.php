<?php
/**
  * Interface HttpException | src/Contracts/HttpException.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Contracts;

/**
 * Interface for all HTTP exceptions to implement.
 *
 * @package     Rmb32\Http
 * @subpackage  Contracts
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
interface HttpException
{
    
}
