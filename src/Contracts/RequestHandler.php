<?php
/**
  * Interface RequestHandler | src/Contracts/RequestHandler.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Contracts;

use Rmb32\Http\Request;
use Rmb32\Http\Response;

/**
 * An interface for classes that can handle HTTP requests and return resopnses.
 *
 * @package     Rmb32\Http
 * @subpackage  Contracts
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
interface RequestHandler
{
    /**
     * Handles the given request and produces a response.
     *
     * @param \Rmb32\Http\Request $request The request.
     * @return \Rmb32\Http\Response A response.
     */
    public function handle(Request $request) : Response;
}
