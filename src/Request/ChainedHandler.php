<?php
/**
 * Class ChainedHandler | src/Request/ChainedHandler.php
 *
 * @license proprietary
 */
declare(strict_types=1);

namespace Rmb32\Http\Request;

use Rmb32\Http\Request;
use Rmb32\Http\Response;
use Rmb32\Http\Contracts\RequestHandler;

/**
 * A chained handler that can pass the request on to
 * the next handler which may or may no also be a
 * chained handler.
 *
 * This class is essentially middleware.
 *
 * @package Rmb32\Http;
 * @subpackage Request;
 * @author Roger Barnfather <roger@rogerbarnfather.com>
 */
abstract class ChainedHandler implements RequestHandler
{
    /**
     * @var \Rmb32\Http\Contracts\RequestHandler $child The child request
     *     handler to be called after this one.
     */
    private $child;

    /**
     * Constructs a new chained handler.
     *
     * @param \Rmb32\Http\Contracts\RequestHandler $child The child request
     *     handler to be called after this one.
     */
    public function __construct(RequestHandler $child)
    {
        $this->child = $child;
    }

    /**
     * Handles a request.
     *
     * @param \Rmb32\Http\Request The request.
     * @return \Rmb32\Http\Response A response.
     */
    abstract public function handle(Request $request) : Response;
    
    /**
     * Passes the request on to the next handler.
     *
     * @param \Rmb32\Http\Request $request The request.
     * @return \Rmb32\Http\Response A response object.
     */
    protected function next(Request $request) : Response
    {
        return $this->child->handle($request);
    }
}
