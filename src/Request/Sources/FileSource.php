<?php
/**
 * Class FileSource | src/Request/Sources/FileSource.php
 *
 * @license Proprietary
 */

declare(strict_types=1);

namespace Rmb32\Http\Request\Sources;

use Rmb32\Http\Header;
use Rmb32\Http\Header\HeaderCollection;
use Rmb32\Http\Contracts\RequestSource;
use Rmb32\Http\Exceptions\HttpException;

/**
 * This class contains methods to provide all the data required to build
 * a request object.
 *
 * The data is taken from a text based file.
 * The file must be formatted exactly a valid HTTP request.
 *
 * @package     Rmb32\Http
 * @subpackage  Request\Sources
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class FileSource implements RequestSource
{
    /**
     * @var string $uriLine The first line of the HTTP request.
     */
    protected $uriLine;

    /**
     * @var array $headers The header strings.
     */
    protected $headers;

    /**
     * @var string $body The request body (posted data).
     */
    protected $body;

    /**
     * Constructs a new FileSource object.
     *
     * @param string $filename The file to write to.
     */
    public function __construct(string $filename)
    {
        if (!file_exists($filename)) {
            throw new HttpException(
                'File does not exist: "' . $filename . '"'
            );
        }
        
        if (!is_readable($filename)) {
            throw new HttpException(
                'File is not readable: "' . $filename . '"'
            );
        }

        $data = file_get_contents($filename);

        $halves = explode("\r\n\r\n", $data);
        $this->body = count($halves) > 1 ? $halves[1] : '';

        $lines = explode("\r\n", $halves[0]);
        $this->uriLine = explode(' ', array_shift($lines));

        $this->headers = $lines;
    }

    /**
     * Gets the HTTP method.
     *
     * @return string The HTTP method.
     */
    public function getMethod() : string
    {
        return $this->uriLine[0];
    }
    
    /**
     * Gets the request URI without the query string.
     *
     * @return string The request URI without the query string.
     */
    public function getUri() : string
    {
        $fullUri = $this->uriLine[1];
        $parts = explode('?', $fullUri);
        
        return $parts[0];
    }
    
    /**
     * Gets the query string.
     *
     * @return string The query string.
     */
    public function getQueryString() : string
    {
        $fullUri = $this->uriLine[1];
        $parts = explode('?', $fullUri);
        
        if (count($parts) === 2) {
            return $parts[1];
        }

        return '';
    }
    
    /**
     * Gets the HTTP version.
     *
     * @return string The HTTP version.
     */
    public function getHttpVersion() : string
    {
        $fullVersion = $this->uriLine[2];
        $numericPart = substr($fullVersion, 5);

        return $numericPart;
    }
    
    /**
    * Gets the HTTP headers.
    *
    * @return \Rmb32\Http\Header\HeaderCollection The headers.
    */
    public function getHeaders() : HeaderCollection
    {
        $headers = new HeaderCollection();
        
        foreach ($this->headers as $h) {
            list($k, $v) = explode(':', $h);

            // Add the new header.
            $headers = $headers->withHeader(new Header(trim($k), trim($v)));
        }
        
        return $headers;
    }
    
    /**
     * Gets the request body (posted data).
     *
     * @return string The request body.
     */
    public function getBody() : string
    {
        return $this->body;
    }
}
