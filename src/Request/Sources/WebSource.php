<?php
/**
 * Class WebSource | src/Request/Sources/WebSource.php
 *
 * @license Proprietary
 */

declare(strict_types=1);

namespace Rmb32\Http\Request\Sources;

use Rmb32\Http\Header;
use Rmb32\Http\Request\Method;
use Rmb32\Http\Header\HeaderCollection;
use Rmb32\Http\Contracts\RequestSource;

/**
 * This class contains methods to provide all the data required to build
 * a request object.
 *
 * The data is taken from PHP's $_SERVER superglobal.
 * If any data was posted, it is taken from the php://input stream.
 *
 * @package     Rmb32\Http
 * @subpackage  Request\Sources
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class WebSource implements RequestSource
{
    /**
     * Gets the HTTP method.
     *
     * @return string The HTTP method.
     */
    public function getMethod() : Method
    {
        return $_SERVER['REQUEST_METHOD'];
    }
    
    /**
     * Gets the request URI without the query string.
     *
     * @return string The request URI without the query string.
     */
    public function getUri() : string
    {
        $fullUri = $_SERVER['REQUEST_URI'];
        
        $uriParts = explode('?', $fullUri);
        
        return reset($uriParts);
    }
    
    /**
     * Gets the query string.
     *
     * @return string The query string.
     */
    public function getQueryString() : string
    {
        return $_SERVER['QUERY_STRING'];
    }
    
    /**
     * Gets the HTTP version.
     *
     * @return string The HTTP version.
     */
    public function getHttpVersion() : string
    {
        return substr($_SERVER['SERVER_PROTOCOL'], -3);
    }
    
    /**
    * Gets the HTTP headers.
    *
    * @return \Rmb32\Http\Request\Header\HeaderCollection The headers.
    */
    public function getHeaders() : HeaderCollection
    {
        $headers = new HeaderCollection;
        
        foreach ($_SERVER as $k => $v) {
            // Take all the "HTTP_" keys in $_SERVER.
            if (substr($k, 0, 5) === 'HTTP_') {
                // Remove the "HTTP_" part.
                $k = substr($k, 5);
                
                // Change underscores to spaces.
                $k = str_replace('_', ' ', $k);
                
                // Lowercase everything and then uppercase word beginnings.
                $k = ucwords(strtolower($k));
                
                // Change the space characters to hyphens.
                $k = str_replace(' ', '-', $k);
                
                // Add the new header.
                $headers = $headers->withHeader(new Header($k, $v));
            }
        }
        
        return $headers;
    }
    
    /**
     * Gets the request body (posted data).
     *
     * @return string The request body.
     */
    public function getBody() : string
    {
        return file_get_contents('php://input');
    }
}
