<?php
/**
  * Class Header | src/Header.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http;

use Serializable;

/**
 * Class for representing an HTTP header.
 *
 * @package     Rmb32\Http
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class Header implements Serializable
{
    /**
     * @var string $name The header name.
     */
    protected $name = '';
    
    /**
     * @var string $value The header value.
     */
    protected $value = '';
    
    /**
     * Constructs a new Header object.
     *
     * @param string $name The header name.
     * @param string $value The header value.
     */
    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        $this->value = $value;
    }
    
    /**
     * Gets the header name.
     *
     * @return string The header name.
     */
    public function getName() : string
    {
        return $this->name;
    }
    
    /**
     * Gets the header value.
     *
     * @return string The header value.
     */
    public function getValue() : string
    {
        return $this->value;
    }
    
    /**
     * Determines if the header name matches the give name.
     *
     * @param string $name The header name to match.
     * @param bool $ignoreCase Whether or not to ignore letter casing.
     * @return bool TRUE if the names match, otherwise FALSE.
     */
    public function is(string $name, bool $ignoreCase = true) : bool
    {
        if ($ignoreCase) {
            return strtolower($this->name) === strtolower($name);
        }
        
        return $this->name === $name;
    }

    /**
     * Gets a serialized string representing this object.
     *
     * @return string A serialized version of this object.
     */
    public function serialize() : string
    {
        return serialize($this->toArray());
    }

    /**
     * Unserializes a serialized Header.
     *
     * @param string $serialized The serialized Header.
     * @return void
     */
    public function unserialize($serialized) : void
    {
        $raw = unserialize($serialized);

        $this->__construct($raw['name'], $raw['value']);
    }
    
    /**
     * Gets an array representation of the header.
     *
     * @return array The array representation of the header.
     *     The letter casing of the name is preserved.
     */
    public function toArray() : array
    {
        return ['name' => $this->name, 'value' => $this->value];
    }
    
    /**
     * Gets a string representation of the header.
     *
     * @return string The string representation of the header.
     *     The letter casing of the name is preserved.
     */
    public function __toString() : string
    {
        return $this->getName() . ': ' . $this->getValue();
    }
}
