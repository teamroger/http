<?php
/**
 * Class HttpException | src/Exceptions/HttpException.php
 *
 * @license Proprietary
 */

declare(strict_types=1);

namespace Rmb32\Http\Exceptions;

use Exception;
use Rmb32\Http\Contracts\HttpException as HttpExceptionInterface;

/**
 * Superclass from which all other HTTP based exceptions will extend.
 *
 * @package     Rmb32\Http
 * @subpackage  Exceptions
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class HttpException extends Exception implements HttpExceptionInterface
{
    
}
