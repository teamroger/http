<?php
/**
  * Class: Request | src/Request.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http;

use Rmb32\Http\Header\HeaderCollection;
use Rmb32\Http\Exceptions\HttpException;

/**
 * Class to represent an incomming HTTP request.
 *
 * @package Rmb32\Http
 * @author  Roger Barnfather <roger@rogerbarnfather.com>
 */
class Request
{
    /**
     * @var string HTTP_1_0 The constant for version 1.0 of HTTP.
     */
    const HTTP_1_0 = '1.0';

    /**
     * @var string HTTP_1_1 The constant for version 1.1 of HTTP.
     */
    const HTTP_1_1 = '1.1';

    /**
     * @var string METHOD_ The constant for the HTTP GET method.
     */
    const METHOD_GET = 'GET';
    
    /**
     * @var string METHOD_ The constant for the HTTP POST method.
     */
    const METHOD_POST = 'POST';
    
    /**
     * @var string METHOD_ The constant for the HTTP PUT method.
     */
    const METHOD_PUT = 'PUT';
    
    /**
     * @var string METHOD_ The constant for the HTTP DELETE method.
     */
    const METHOD_DELETE = 'DELETE';
    
    /**
     * @var string METHOD_ The constant for the HTTP HEAD method.
     */
    const METHOD_HEAD = 'HEAD';
    
    /**
     * @var string METHOD_ The constant for the HTTP TRACE method.
     */
    const METHOD_TRACE = 'TRACE';
    
    /**
     * @var string METHOD_ The constant for the HTTP OPTIONS method.
     */
    const METHOD_OPTIONS = 'OPTIONS';
    
    /**
     * @var string METHOD_ The constant for the HTTP CONNECT method.
     */
    const METHOD_CONNECT = 'CONNECT';

    /**
     * @var string $method The HTTP method e.g.- "GET".
     */
    protected $method;

    /**
     * @var string $uri The request URI excluding the query string.
     */
    protected $uri;

    /**
     * @var string $queryString The query string.
     */
    protected $queryString;

    /**
     * @var string $httpVersion The HTTP version.
     */
    protected $httpVersion;
    
    /**
     * @var \Rmb32\Http\Header\HeaderCollection $headers The HTTP headers.
     */
    protected $headers;

    /**
     * @var string $body The request body (posted data).
     */
    protected $body;
    
    /**
     * Constructs a new HTTP Request object.
     *
     * @param string $method The HTTP request method.
     * @param string $uri The request URI without the query string.
     * @param string $queryString The query string.
     * @param string $httpVersion The HTTP version. This should be one of
     *     the class HTTP constants e.g.- Request::HTTP_1_1.
     * @param \Rmb32\Http\Header\HeaderCollection $headers The HTTP headers.
     * @param string $body The request body (posted data).
     *
     * @throws \Rmb32\Http\Exceptions\HttpException On invalid HTTP version.
     */
    private function __construct(
        string $method,
        string $uri,
        string $queryString,
        string $httpVersion,
        HeaderCollection $headers,
        string $body
    ) {
        $this->method      = $method;
        $this->uri         = $uri;
        $this->queryString = $queryString;
        $this->httpVersion = $httpVersion;
        $this->headers     = $headers;
        $this->body        = $body;
    }
    
    /**
     * Creates a new HTTP Request object.
     *
     * @param string $method The HTTP request method.
     * @param string $uri The request URI without the query string.
     * @param string $queryString The query string.
     * @param string $httpVersion The HTTP version. This should be one of
     *     the class HTTP constants e.g.- Request::HTTP_1_1.
     * @param \Rmb32\Http\Header\HeaderCollection $headers The HTTP headers.
     * @param string $body The request body (posted data).
     *
     * @throws \Rmb32\Http\Exceptions\HttpException On invalid HTTP version.
     */
    public static function create(
        string $method,
        string $uri,
        string $queryString,
        string $httpVersion,
        HeaderCollection $headers,
        string $body
    ) : Request {
        if (!in_array(
            strtoupper($method),
            [
                self::METHOD_CONNECT, self::METHOD_DELETE,
                self::METHOD_GET,     self::METHOD_HEAD,
                self::METHOD_OPTIONS, self::METHOD_POST,
                self::METHOD_PUT,     self::METHOD_TRACE
            ]
        )) {
            throw new HttpException(
                'Ivalid HTTP method "' . $method . '"'
            );
        }

        if (self::HTTP_1_0 !== $httpVersion
            && self::HTTP_1_1 !== $httpVersion
        ) {
            throw new HttpException(
                'Invalid HTTP version: "' . $httpVersion . '"'
            );
        }

        return new Request(
            $method,
            $uri,
            $queryString,
            $httpVersion,
            $headers,
            $body
        );
    }

    /**
     * Get the request method.
     *
     * @return string The request method.
     */
    public function getMethod() : string
    {
        return $this->method;
    }
    
    /**
     * Gets the request URI without the query string.
     *
     * @return string The request URI.
     */
    public function getUri() : string
    {
        return $this->uri;
    }
    
    /**
     * Gets the query string.
     *
     * @return string The query string.
     */
    public function getQueryString() : string
    {
        return $this->queryString;
    }
    
    /**
     * Gets the HTTP version.
     *
     * @return string The HTTP version.
     */
    public function getHttpVersion() : string
    {
        return $this->httpVersion;
    }
    
    /**
     * Gets the headers.
     *
     * @return \Rmb32\Http\Header\HeaderCollection The HTTP headers.
     */
    public function getHeaders() : HeaderCollection
    {
        return $this->headers;
    }
    
    /**
     * Gets the request body (posted data)
     *
     * @return string The request body (posted data).
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * Gets the query string variables.
     *
     * @return array The query string variables as an associative array.
     */
    public function getQueryVars() : array
    {
        $vars = [];
        parse_str($this->queryString, $vars);

        return $vars;
    }
    
    /**
     * Gets the body variables (posted data).
     *
     * @return array The body variables (posted data) as an associative array.
     */
    public function getBodyVars() : array
    {
        $vars = [];
        parse_str($this->body, $vars);

        return $vars;
    }
    
    /**
     * Gets a string representation of this request object.
     * The string will be formatted as a valid HTTP request.
     * If the data given to construct this object was valid
     * then the string should be a valid, useable HTTP request.
     *
     * @return string The HTTP request as a string.
     */
    public function __toString()
    {
        $queryString = strlen($this->getQueryString()) > 0
            ? ('?' . $this->getQueryString()) : '';

        return implode(
            "\r\n",
            [
                implode(
                    ' ',
                    [
                        $this->getMethod(),
                        $this->getUri() . $queryString,
                        'HTTP/' . $this->getHttpVersion(),
                    ]
                ),
                $this->getHeaders(),
                $this->getBody(),
            ]
        );
    }
}
