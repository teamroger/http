<?php
/**
  * Class Response | src/Response.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http;

use Rmb32\Http\Header\HeaderCollection;
use Rmb32\Http\Exceptions\HttpException;

/**
 * Class to represent an HTTP response.
 *
 * @package     Rmb32\Http
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class Response
{
    /**
     * @var string HTTP_1_0 The constant for version 1.0 of HTTP.
     */
    const HTTP_1_0 = '1.0';
    
    /**
     * @var string HTTP_1_1 The constant for version 1.1 of HTTP.
     */
    const HTTP_1_1 = '1.1';
    
    /**
     * @var string $httpVersion The HTTP version for the response.
     */
    protected $httpVersion;
    
    /**
     * @var integer $code The HTTP response code to use e.g.- 404.
     */
    protected $code;
    
    /**
     * @var string $phrase The response phrase e.g.- "Not found".
     */
    protected $phrase;
    
    /**
     * @var \Rmb32\Http\Header\HeaderCollection $headers The headers to send.
     */
    protected $headers;
    
    /**
     * @var string $body The main part of the response. The body text.
     */
    protected $body;
    
    /**
     * Constructs a new Response object.
     *
     * @param string $body The HTTP response body, typically some HTML.
     * @param \Rmb32\Http\Header\HeaderCollection $headers The headers to send.
     * @param string $httpVersion The HTTP version.
     * @param int $code The HTTP status code.
     * @param string $phrase The human readable response phrase.
     */
    public function __construct(
        string $body,
        HeaderCollection $headers,
        string $httpVersion = self::HTTP_1_1,
        int $code = 200,
        string $phrase = 'OK'
    ) {
        $this->body = $body;
        $this->headers = $headers;
        $this->httpVersion = $httpVersion;
        $this->code = $code;
        $this->phrase = $phrase;
    }
    
    /**
     * Sets the HTTP version.
     *
     * @param string $version The HTTP version. This should be set using one
     *     of the Response class HTTP constants e.g.- Response::HTML_1_1.
     * @return \Rmb32\Http\Response This object.
     */
    public function setHttpVersion(string $version) : Response
    {
        if (self::HTTP_1_0 !== $version && self::HTTP_1_1 !== $version) {
            throw new HttpException(
                'Invalid HTTP version: "' . $version . '"'
            );
        }
        
        $this->httpVersion = $version;
        
        return $this;
    }
    
    /**
     * Gets the HTTP version.
     *
     * @return string The HTTP version.
     */
    public function getHttpVersion() : string
    {
        return $this->httpVersion;
    }
    
    /**
     * Sets the HTTP status code.
     *
     * @param int $code The HTTP status code e.g.- 404.
     * @return \Rmb32\Http\Response This object.
     */
    public function setCode(int $code) : Response
    {
        if ($code >= 100 && $code < 600) {
            $this->code = $code;
            
            return $this;
        } else {
            throw new HttpException(
                'Invalid response code set: "' . $code . '"'
            );
        }
    }
    
    /**
     * Gets the HTTP status code.
     *
     * @return string The HTTP status code.
     */
    public function getCode() : int
    {
        return $this->code;
    }
    
    /**
     * Sets the human readable response phrase.
     *
     * @param string $phrase The human readable response phrase e.g- "OK".
     * @return \Rmb32\Http\Response This object.
     */
    public function setPhrase(string $phrase) : Response
    {
        $this->phrase = $phrase;
        
        return $this;
    }
    
    /**
     * Gets the human readable response phrase.
     *
     * @return string The human readable response phrase.
     */
    public function getPhrase() : string
    {
        return $this->phrase;
    }
    
    /**
     * Gets the first line of the HTTP response (the resopnse line).
     *
     * @return string The response line.
     */
    public function getResponseLine() : string
    {
        return implode(
            ' ',
            [
                'HTTP/' . $this->getHttpVersion(),
                $this->getCode(),
                $this->getPhrase(),
            ]
        );
    }
    
    /**
     * Gets the HTTP headers.
     *
     * @return \Rmb32\Http\Header\HeaderCollection The headers.
     */
    public function getHeaderCollection() : HeaderCollection
    {
        return $this->headers;
    }
    
    /**
     * Sets the response body text.
     *
     * @param string $body The main body text, typically some HTML.
     * @return \Rmb32\Http\Response This object.
     */
    public function setBody(string $body) : Response
    {
        $this->body = $body;
        
        return $this;
    }
    
    /**
     * Gets the response body.
     *
     * @return string The response body.
     */
    public function getBody() : string
    {
        return $this->body;
    }
    
    /**
     * Appends text to the main body text.
     *
     * @param string $bodyData The text to append to the body.
     * @return \Rmb32\Http\Response This object.
     */
    public function append(string $bodyData) : Response
    {
        $this->body .= $bodyData;
        
        return $this;
    }
    
    /**
     * Gets a string representation of this response.
     *
     * @return string A string representation of this response.
     */
    public function __toString() : string
    {
        return implode(
            "\r\n",
            [
                $this->getResponseLine(),
                $this->headers, // HeaderCollection ends with "\r\n"
                $this->getBody(),
            ]
        );
    }
}
