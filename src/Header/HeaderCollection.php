<?php
/**
 * Class HeaderCollection | src/Header/HeaderCollection.php
 *
 * @license Proprietary
 */

declare(strict_types=1);

namespace Rmb32\Http\Header;

use Iterator;
use Countable;
use Rmb32\Http\Exceptions\HttpException;
use Serializable;
use Rmb32\Http\Header;

/**
 * Class to represent a collection of HTTP headers.
 *
 * This class is immutable so new instances are returned by the
 * updating methods.
 *
 * @package     Rmb32\Http
 * @subpackage  Header
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class HeaderCollection implements Iterator, Countable, Serializable
{
    /**
     * @var array $headers HTTP header objects.
     */
    protected $headers = [];

    /**
     * Constructs a new HeaderCollection object.
     *
     * @param array $headers an array of Header objects.
     * @throws \Rmb32\Http\Exception\HttpException If any of the items
     *     in the given array are not Header objects.
     */
    public function __construct(array $headers = [])
    {
        foreach ($headers as $header) {
            if (!$header instanceof Header) {
                throw new HttpException('Invalid header given');
            }

            $this->headers[] = $header;
        }
    }

    /**
     * Adds a header to this collection.
     *
     * @param \Rmb32\Http\Header $header The header to add.
     *     Duplicates are permitted.
     * @return \Rmb32\Http\Header\HeaderCollection A new collection
     *     containing all previous headers and the new one.
     */
    public function withHeader(Header $header) : HeaderCollection
    {
        $collection = new HeaderCollection();

        $collection->headers = $this->headers;
        $collection->headers[] = $header;

        return $collection;
    }

    /**
     * Removes a header from this collection.
     *
     * @param \Rmb32\Http\Header $header The header to remove.
     * @return \Rmb32\Http\Header\HeaderCollection A new collection
     *     containing all previous headers except for the given one.
     */
    public function withoutHeader(Header $header) : HeaderCollection
    {
        $collection = new HeaderCollection();

        foreach ($this->headers as $h) {
            if ($h !== $header) {
                $collection->headers[] = $h;
            }
        }

        return $collection;
    }

    /**
     * Determines if this collection contains a header with a certain name.
     *
     * @param string $name The name of the header.
     * @return bool TRUE if it does, otherwise FALSE.
     */
    public function has(string $name) : bool
    {
        foreach ($this->headers as $header) {
            if ($header->is($name)) {
                return true;
            }

            return false;
        }
    }

    /**
     * Gets the number of headers in the collection.
     *
     * @return integer The number of headers in the collection.
     */
    public function count()
    {
        return count($this->headers);
    }

    /**
     * Gets the current item when iterating this object.
     *
     * @return Header The current item.
     */
    public function current() : Header
    {
        return current($this->headers);
    }

    /**
     * Gets the key of the current item when iterating this object.
     *
     * @return mixed The key of the current item.
     */
    public function key()
    {
        return key($this->headers);
    }

    /**
     * Sets the internal pointer to the next item when iterating this object.
     *
     * @return void
     */
    public function next() : void
    {
        next($this->headers);
    }

    /**
     * Resets the internal pointer to the start when iterating this object.
     *
     * @return void
     */
    public function rewind() : void
    {
        reset($this->headers);
    }

    /**
     * Determines if the internal pointer is currently at a valid location.
     *
     * @return boolean TRUE if it is valid, otherwise FALSE.
     */
    public function valid() : bool
    {
        return key($this->headers) !== null;
    }

    /**
     * Gets a serialized string representing this object.
     *
     * @return string A serialized version of this object.
     */
    public function serialize() : string
    {
        return serialize(
            array_map(function (Header $header) : array {
                return $header->toArray();
            }, $this->headers)
        );
    }

    /**
     * Unserializes a serialized HeaderCollection.
     *
     * @param string $serialized The serialized HeaderCollection.
     * @return void
     */
    public function unserialize($serialized) : void
    {
        $this->headers = array_map(function (array $header) : Header {
            return new Header($header['name'], $header['value']);
        }, unserialize($serialized));
    }

    /**
     * Gets an array representation of this collection.
     *
     * @return array The array representation of this collection.
     */
    public function toArray() : array
    {
        return array_map(function ($header) {
            return $header;
        }, $this->headers);
    }

    /**
     * Gets a string representation of this collection.
     *
     * @return string The string representation of this collection.
     */
    public function __toString() : string
    {
        return implode("\r\n", $this->headers) . "\r\n";
    }
}
