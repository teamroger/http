<?php
/**
  * Class: WebDestination | src/Response/WebDestination.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Response\Destinations;

use Rmb32\Http\Header;
use Rmb32\Http\Response;
use Rmb32\Http\Contracts\ResponseDestination;

/**
 * Class to represent an HTTP client as a final destination
 * for an HTTP response.
 *
 * @package     Rmb32\Http
 * @subpackage  Response\Destinations
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class WebDestination implements ResponseDestination
{
    /**
     * Takes an HTTP response and outputs it to the web client.
     *
     * @param \Rmb32\Http\Response $response The HTTP response.
     * @return void
     */
    public function accept(Response $response) : void
    {
        header($response->getResponseLine());
        
        array_walk($response->getHeaders(), function (Header $header) {
            header($header);
        });
        
        echo $response->getBody();
    }
}
