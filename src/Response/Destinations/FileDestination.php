<?php
/**
  * Class: FileDestination | src/Response/FileDestination.php
  *
  * @license Proprietary
  */

declare(strict_types=1);

namespace Rmb32\Http\Response\Destinations;

use Rmb32\Http\Response;
use Rmb32\Http\Exceptions\HttpException;
use Rmb32\Http\Contracts\ResponseDestination;

/**
 * Class to represent a text based file as a final destination
 * for an HTTP response.
 *
 * @package     Rmb32\Http
 * @subpackage  Response\Destinations
 * @author      Roger Barnfather <roger@rogerbarnfather.com>
 */
class FileDestination implements ResponseDestination
{
    /**
     * @var string $filename The file to write the response to.
     */
    protected $filename;

    /**
     * Constructs a new FileDestination object.
     *
     * @param string $filename The file to write to.
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * Takes an HTTP response and outputs it to the file.
     *
     * @param \Rmb32\Http\Response $response The HTTP response.
     * @return void
     */
    public function accept(Response $response) : void
    {
        $dirname = dirname($this->filename);
        $dir = realpath($dirname);

        if (false === $dir) {
            throw new HttpException(
                'Directory does not exist for file to be written to: "'
                . $dirname . '"'
            );
        }

        if (!is_writable($dir)) {
            throw new HttpException(
                'Directory is not writable: "' . $dir . '"'
            );
        }

        if (file_exists($this->filename)) {
            if (!is_writable($this->filename)) {
                throw new HttpException(
                    'File exists and is not writable: "' . $this->filename . '"'
                );
            }
        }

        file_put_contents($this->filename, (string)$response);
    }
}
