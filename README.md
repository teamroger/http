# Rmb32\Http

## An HTTP library

This library provides the basic building blocks for dealing with HTTP requests and responses.

### Documentation

To generate PHPDoc documentation PHPDocumentor is needed. There is a PHAR file available to run with the command:
```
php phpdoc -d ./src -t ./docs --template="responsive-twig"
```

Alternatively there is a composer package available but this may conflict with PHPUnit which requires a dependency in common with PHPDocumentor at a different version.
