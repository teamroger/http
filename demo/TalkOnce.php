<?php

declare(strict_types=1);

namespace Demo;

use Rmb32\Http\Request;
use Rmb32\Http\Response;
use Rmb32\Http\Request\ChainedHandler;

class TalkOnce extends ChainedHandler
{
    public function handle(Request $request) : Response
    {
        echo "\nfoo\n";
        $response = $this->next($request);
        echo "\nbar\n";

        return $response;
    }
}
