<?php declare(strict_types=1);

require_once '../vendor/autoload.php';

require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . 'Handler.php';
require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . 'TalkOnce.php';
require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . 'TalkTwice.php';

use Demo\Handler;
use Demo\TalkOnce;
use Demo\TalkTwice;
use Rmb32\Http\Request\Builder;
use Rmb32\Http\Request\Sources\FileSource;
use Rmb32\Http\Response\Destinations\FileDestination;

echo "\nbuilding\n";
$builder = new Builder;
$request = $builder->build(new FileSource('request.txt'));

echo "\nhandling\n";
$handler = new Handler('template.html');
$secondware = new TalkTwice($handler);
$firstware = new TalkOnce($secondware);
$response = $firstware->handle($request);

echo "\nresponding\n";
$destination = new FileDestination('response.txt');
$destination->accept($response);

/*
$req  = "GET /images/cool?letter=y HTTP/1.1\r\n";
$req .= "Accept: application\json\r\n";
$req .= "Other-header: other\r\n";
$req .= "Third-one: IamThird\r\n";
$req .= "\r\n";
$req .= "posted=1&data=2&please=3";

file_put_contents('request.txt', $req);
echo "\ndone\n";
*/
