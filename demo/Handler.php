<?php declare(strict_types=1);

namespace Demo;

use Rmb32\Http\Header;
use Rmb32\Http\Request;
use Rmb32\Http\Response;
use Rmb32\Http\Header\HeaderCollection;
use Rmb32\Http\Contracts\RequestHandler;

class Handler implements RequestHandler
{
    protected $templatePath;

    public function __construct(string $templatePath)
    {
        $this->templatePath = $templatePath;
    }

    public function handle(Request $request) : Response
    {
        $headers = $request->getHeaders();
        $headerHtml = $this->getHeaderHtml($headers->toArray());

        $details = [
            'method' => $request->getMethod(),
            'uri' => $request->getUri(),
            'queryString' => $request->getQueryString(),
            'headers' => $headerHtml,
        ];

        $template = $this->parseTemplate($this->templatePath, $details);

        return $this->getResponse($template);
    }

    protected function getHeaderHtml(array $headers) : string
    {
        if (count($headers)) {
            $headerList = implode("\n", array_map(function (Header $header) {
                return "<li>$header</li>";
            }, $headers));

            return "<ul>$headerList</ul>";
        }

        return 'none';
    }

    protected function parseTemplate(string $filename, array $vars) : string
    {
        $template = file_get_contents($filename);
        
        foreach ($vars as $key => $value) {
            $template = str_replace('{' . $key . '}', $value, $template);
        }
        
        return $template;
    }

    protected function getResponse(string $body) : Response
    {
        $headers = (new HeaderCollection([
            new Header('Content-type', 'text/html'),
            new Header('Content-length', (string) strlen($body)),
        ]));
        
        return new Response(
            $body,
            $headers
        );
    }
}
