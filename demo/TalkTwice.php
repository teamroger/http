<?php

declare(strict_types=1);

namespace Demo;

use Rmb32\Http\Request;
use Rmb32\Http\Response;
use Rmb32\Http\Request\ChainedHandler;

class TalkTwice extends ChainedHandler
{
    public function handle(Request $request) : Response
    {
        echo "\nbaz\n";
        $response = $this->next($request);
        echo "\nqux\n";

        return $response;
    }
}
